# Using docker-compose on Bitbucket Pipelines

[docker-compose](https://docs.docker.com/compose/) is a python
application and we can install it via
[pip](https://pip.pypa.io/en/stable/).

We use [Docker in Docker](https://hub.docker.com/_/docker/) as
_custom_ image in our build.

We can issue `docker-compose [...]` commands without any issue.

## Example build

This example uses 2 containers a _server_ and a _client_ (reusing the
same image).  The server takes some time to start, so the client waits
to be ready.
